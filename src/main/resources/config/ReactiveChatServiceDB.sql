-- phpMyAdmin SQL Dump
-- version 4.8.2
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Creato il: Ago 12, 2018 alle 22:18
-- Versione del server: 10.1.34-MariaDB
-- Versione PHP: 7.2.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ReactiveChatService`
--
CREATE DATABASE IF NOT EXISTS `ReactiveChatService` DEFAULT CHARACTER SET latin1 COLLATE latin1_bin;
USE `ReactiveChatService`;

-- --------------------------------------------------------

--
-- Struttura della tabella `Chat`
--
-- Creazione: Ago 12, 2018 alle 20:12
--

CREATE TABLE IF NOT EXISTS `Chat` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` text COLLATE latin1_bin NOT NULL,
  `address` text COLLATE latin1_bin,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`(16))
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1 COLLATE=latin1_bin;

--
-- RELAZIONI PER TABELLA `Chat`:
--

--
-- Dump dei dati per la tabella `Chat`
--

INSERT INTO `Chat` (`id`, `name`, `address`) VALUES
(1, 'Default Chat', NULL),
(2, 'Meme Shitposting', NULL);

-- --------------------------------------------------------

--
-- Struttura della tabella `ChatParticipant`
--
-- Creazione: Lug 17, 2018 alle 19:34
--

CREATE TABLE IF NOT EXISTS `ChatParticipant` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user` int(11) NOT NULL,
  `chat` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `user` (`user`),
  KEY `chat` (`chat`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1 COLLATE=latin1_bin;

--
-- RELAZIONI PER TABELLA `ChatParticipant`:
--   `chat`
--       `Chat` -> `id`
--   `user`
--       `User` -> `id`
--

--
-- Dump dei dati per la tabella `ChatParticipant`
--

INSERT INTO `ChatParticipant` (`id`, `user`, `chat`) VALUES
(1, 1, 1),
(2, 2, 1),
(3, 3, 1);

-- --------------------------------------------------------

--
-- Struttura della tabella `Messages`
--
-- Creazione: Lug 22, 2018 alle 14:17
--

CREATE TABLE IF NOT EXISTS `Messages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `text` text COLLATE latin1_bin NOT NULL,
  `user` int(11) NOT NULL,
  `chat` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `user_fk` (`user`),
  KEY `chat_fk` (`chat`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1 COLLATE=latin1_bin;

--
-- RELAZIONI PER TABELLA `Messages`:
--   `chat`
--       `Chat` -> `id`
--   `user`
--       `User` -> `id`
--

--
-- Dump dei dati per la tabella `Messages`
--

INSERT INTO `Messages` (`id`, `date`, `text`, `user`, `chat`) VALUES
(1, '2018-08-12 20:15:22', 'Hello everyone!', 1, 1);

-- --------------------------------------------------------

--
-- Struttura della tabella `User`
--
-- Creazione: Lug 17, 2018 alle 19:39
--

CREATE TABLE IF NOT EXISTS `User` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` text COLLATE latin1_bin NOT NULL,
  `email` text COLLATE latin1_bin NOT NULL,
  `password` text COLLATE latin1_bin NOT NULL,
  `address` text COLLATE latin1_bin NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`(8)),
  UNIQUE KEY `email` (`email`(32))
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1 COLLATE=latin1_bin;

--
-- RELAZIONI PER TABELLA `User`:
--

--
-- Dump dei dati per la tabella `User`
--

INSERT INTO `User` (`id`, `username`, `email`, `password`, `address`) VALUES
(1, 'Rei', 'rei.ayanami@gmail.com', 'eva', 'localhost:8081'),
(2, 'Asuka', 'asuka.langely@gmail.com', 'eva', 'localhost:8081'),
(3, 'Shinji', 'shinji.ikari@gmail.com', 'eva', 'localhost:8081');

--
-- Limiti per le tabelle scaricate
--

--
-- Limiti per la tabella `ChatParticipant`
--
ALTER TABLE `ChatParticipant`
  ADD CONSTRAINT `chat` FOREIGN KEY (`chat`) REFERENCES `Chat` (`id`),
  ADD CONSTRAINT `user` FOREIGN KEY (`user`) REFERENCES `User` (`id`);

--
-- Limiti per la tabella `Messages`
--
ALTER TABLE `Messages`
  ADD CONSTRAINT `chat_fk` FOREIGN KEY (`chat`) REFERENCES `Chat` (`id`),
  ADD CONSTRAINT `user_fk` FOREIGN KEY (`user`) REFERENCES `User` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
