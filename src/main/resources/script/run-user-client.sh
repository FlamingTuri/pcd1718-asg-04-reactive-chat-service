#!/bin/bash

# npm start is to configure

re='^[0-9]+$'
if [[ $1 =~ $re ]] ; then
  cd '../webroot/user-service'
  ng serve -o --port $1
else
  echo "error: arg '$1' is not a number" >&2; exit 1
fi
