import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ReactiveChatServiceApiService {

  private port: string = 'http://127.0.0.1:8080';

  private userApi: string = '/user';
  private chatsApi: string = '/chats'
  private chatApi: string = '/chat';

  userRegistrationApi: string = this.userApi + '/register';
  userLoginApi: string = this.userApi + '/login';
  userLogoutApi: string = this.userApi + '/logout';

  availableChatsApi: string = this.chatsApi + '/availableList';
  subscribedChatsApi: string = this.chatsApi + '/subscribedList';

  readyApi: string = this.chatApi + '/ready';
  createChatApi: string = this.chatApi + '/create';
  enterChatApi: string = this.chatApi + '/enter';
  leaveChatApi: string = this.chatApi + '/leave';
  getChatMessagesApi: string = this.chatApi + '/messages';

  constructor() { }

  getServerPath(restApiPath: string): string {
    return this.port + restApiPath;
  }
}
