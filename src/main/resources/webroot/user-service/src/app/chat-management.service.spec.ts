import { TestBed, inject } from '@angular/core/testing';

import { ChatManagementService } from './chat-management.service';

describe('ChatManagementService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ChatManagementService]
    });
  });

  it('should be created', inject([ChatManagementService], (service: ChatManagementService) => {
    expect(service).toBeTruthy();
  }));
});
