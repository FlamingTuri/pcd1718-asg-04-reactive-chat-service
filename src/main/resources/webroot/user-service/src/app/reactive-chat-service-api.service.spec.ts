import { TestBed, inject } from '@angular/core/testing';

import { ReactiveChatServiceApiService } from './reactive-chat-service-api.service';

describe('ReactiveChatServiceApiService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ReactiveChatServiceApiService]
    });
  });

  it('should be created', inject([ReactiveChatServiceApiService], (service: ReactiveChatServiceApiService) => {
    expect(service).toBeTruthy();
  }));
});
