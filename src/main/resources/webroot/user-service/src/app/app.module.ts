import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { HttpClientModule } from '@angular/common/http';
import { BsModalModule } from 'ng2-bs3-modal';

import { AppComponent } from './app.component';
import { LoginComponent } from './login-registration/login/login.component';
import { MainMenuComponent } from './main-menu/main-menu.component';
import { AvailableChatsComponent } from './main-menu/available-chats/available-chats.component';
import { YourChatsComponent } from './main-menu/your-chats/your-chats.component';
import { LoginRegistrationComponent } from './login-registration/login-registration.component';
import { RegisterComponent } from './login-registration/register/register.component';
import { ChatComponent } from './main-menu/your-chats/chat/chat.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    MainMenuComponent,
    AvailableChatsComponent,
    YourChatsComponent,
    LoginRegistrationComponent,
    RegisterComponent,
    ChatComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    HttpClientModule,
    BsModalModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
