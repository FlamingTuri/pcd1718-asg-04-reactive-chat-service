import { Component, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';

import { ChatManagementService } from './chat-management.service';
import { UserSessionService } from './user-session.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers: [
    ChatManagementService
  ],
})
export class AppComponent implements OnDestroy {

  private logInSubscription: Subscription;
  private isMainMenuToShow: boolean = false;
  private isLoginToShow: boolean = true;

  constructor(
    private userSessionService: UserSessionService
  ) {
    this.logInSubscription = userSessionService.selectedData$.subscribe(
      data => {
        // console.log(data);
        this.isMainMenuToShow = true;
        this.isLoginToShow = false;
    });
  }

  ngOnDestroy() {
    this.logInSubscription.unsubscribe;
  }

  backToLoginScreen(){
    this.isMainMenuToShow = false;
    this.isLoginToShow = true;
  }
}
