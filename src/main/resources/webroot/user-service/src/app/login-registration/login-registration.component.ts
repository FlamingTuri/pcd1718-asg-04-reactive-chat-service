import { Component, OnInit } from '@angular/core';
declare var jQuery:any;

@Component({
  selector: 'app-login-registration',
  templateUrl: './login-registration.component.html',
  styleUrls: ['./login-registration.component.css']
})
export class LoginRegistrationComponent implements OnInit {

  private isLoginFormToShow: boolean = true;
  
  constructor() { }

  ngOnInit() {
  }

  displayLoginForm() {
    this.isLoginFormToShow = true;
    //jQuery("#login-form").delay(100).fadeIn(100);
    //jQuery("#register-form").fadeOut(100);
  }

  displayRegisterForm(event) {
    this.isLoginFormToShow = false;
    //jQuery("#register-form").delay(100).fadeIn(100);
    //jQuery("#login-form").fadeOut(100);
  }

}
