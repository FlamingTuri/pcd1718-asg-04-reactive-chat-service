import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

import { UserSessionService } from '../../user-session.service';
declare var jQuery: any;

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
  providers: [],
})
export class LoginComponent implements OnInit {

  private credential: string;
  private password: string;

  constructor(
    private userSessionService: UserSessionService,
  ) { }

  ngOnInit() {
    jQuery("#login-form").delay(100).fadeIn(100);
    jQuery("#register-form").fadeOut(100);
  }

  logIn() {
    if (this.credential != null && this.password != null) {
      this.userSessionService.login(this.credential, this.password)
        .subscribe(response => {
          if (response != "error") {
            this.userSessionService.publishData(response.email, response.username);
          }
          // console.log(response);
        });
    }
  }
}
