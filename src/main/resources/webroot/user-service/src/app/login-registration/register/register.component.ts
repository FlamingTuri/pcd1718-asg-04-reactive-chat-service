import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { User } from '../../user';
import { UserSessionService } from '../../user-session.service';
declare var jQuery: any;

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  user: User;
  confirmPassword: string;
  registerForm: FormGroup;

  constructor(
    private userSessionService: UserSessionService
  ) { }

  ngOnInit() {
    jQuery("#register-form").delay(100).fadeIn(100);
    jQuery("#login-form").fadeOut(100);
    this.user = new User();
    /*this.registerForm = new FormGroup({
      'username': new FormControl(this.user.username, [
        Validators.required,
        Validators.minLength(3),
      ]),
      'email': new FormControl(this.user.email),
      'password': new FormControl(this.user.password, [
        Validators.required,
        Validators.minLength(8),
      ])
    });*/
  }

  register() {
    if (this.user.password == this.confirmPassword) {
      this.user.address = document.location.host;
      console.log(this.user);
      this.userSessionService.register(this.user)
      .subscribe(response => {
        if (response != "error") {
          this.userSessionService.publishData(response.email, response.username);
        }
        console.log(response);
      });
    } else {
      console.log("password field != confirm password field");
    }
  }
}
