import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Observable, Subject, of } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';

import { ReactiveChatServiceApiService } from './reactive-chat-service-api.service';
import { User } from './user';

@Injectable({
  providedIn: 'root'
})
export class UserSessionService {

  private selectedData = new Subject<[string, string]>();
  public selectedData$ = this.selectedData.asObservable();
  private username: string;
  private email: string;

  constructor(
    private http: HttpClient,
    private reactiveChatServiceApi: ReactiveChatServiceApiService
  ) { }

  getUsername(): string {
    return this.username;
  }

  getEmail(): string {
    return this.email;
  }

  publishData(email, username) {
    let data: [string, string] = [email, username];
    this.selectedData.next(data);
  }

  register(user: User): Observable<any> {
    let url = this.reactiveChatServiceApi.getServerPath(this.reactiveChatServiceApi.userRegistrationApi);
    let headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    let options: { headers, responseType: 'json' };
    return this.http.post(url, user, options).pipe(
      tap(response => console.log(response)),
      catchError(this.handleError<any>('authentication', "error"))
    );
  }

  login(loginCredential: string, password: string): Observable<any> {
    let userCredentials = {
      loginCredential: loginCredential,
      password: password
    }

    let url = this.reactiveChatServiceApi.getServerPath(this.reactiveChatServiceApi.userLoginApi);
    let headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    let options: { headers, responseType: 'text' };

    return this.http.post(url, userCredentials, options).pipe(
      tap(response => {
        //console.log(response);
        // double conversion or the compiler will consider it as an error
        let jsonResponse = JSON.parse(JSON.stringify(response));
        this.username = jsonResponse.username;
        this.email = jsonResponse.email;
        return of('success');
      }),
      catchError(this.handleError<any>('authentication', "error"))
    );
  }

  logout(): Observable<any> {
    let url = this.reactiveChatServiceApi.getServerPath(this.reactiveChatServiceApi.userLogoutApi);
    let headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    let options: { headers, responseType: 'json' };
    return this.http.post(url, this.email, options).pipe(
      tap(response => console.log(response)),
      catchError(this.handleError<any>('authentication', "error"))
    );
  }

  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // TODO: better job of transforming error for user consumption
      console.log(`${operation} failed: ${error.message}`);

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }
}
