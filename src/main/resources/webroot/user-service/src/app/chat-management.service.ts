import { Injectable } from '@angular/core';
import { Response } from '@angular/http';
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Observable, of } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';

import { ReactiveChatServiceApiService } from './reactive-chat-service-api.service';

@Injectable({
  providedIn: 'root'
})
export class ChatManagementService {

  headers = new HttpHeaders({ 'Content-Type': 'application/json' });
  options: { headers, responseType: 'json' };

  constructor(
    private http: HttpClient,
    private reactiveChatServiceApi: ReactiveChatServiceApiService
  ) { }

  getAvailableChat(username): Observable<any> {
    let url = this.reactiveChatServiceApi.getServerPath(this.reactiveChatServiceApi.availableChatsApi);
    let user = {
      username: username
    }
    return this.http.post(url, user, this.options).pipe(
      tap((response: Response) => {
        // console.log(response);
        return response;
      }),
      catchError(this.handleError<any>('getAvailableChat', "error"))
    );
  }

  getUserChat(username): Observable<any> {
    let url = this.reactiveChatServiceApi.getServerPath(this.reactiveChatServiceApi.subscribedChatsApi);
    let user = {
      username: username
    }
    return this.http.post(url, user, this.options).pipe(
      tap((response: Response) => {
        // console.log(response);
        // console.log(response[0].name);
        return response;
      }),
      catchError(this.handleError<any>('getUserChat', "error"))
    );
  }

  createChat(newChatData): Observable<any> {
    let url = this.reactiveChatServiceApi.getServerPath(this.reactiveChatServiceApi.createChatApi);
    return this.http.post(url, newChatData, this.options).pipe(
      tap(response => {
        // console.log(response)
      }),
      catchError(this.handleError('createChat', "error"))
    );
  }

  subscribeToChat(chat, username): Observable<any> {
    let url = this.reactiveChatServiceApi.getServerPath(this.reactiveChatServiceApi.enterChatApi);
    let data = {
      chatname: chat.name,
      username: username
    }
    return this.http.post(url, data, this.options).pipe(
      tap(response => {
        // console.log(response);
      }),
      catchError(this.handleError('createChat', "error"))
    );
  }

  leaveChat(chat, username): Observable<any> {
    let url = this.reactiveChatServiceApi.getServerPath(this.reactiveChatServiceApi.leaveChatApi);
    let data = {
      chatname: chat.name,
      username: username
    };
    return this.http.post(url, data, this.options).pipe(
      tap(response => {
        // console.log(response);
      }),
      catchError(this.handleError('createChat', "error"))
    );
  }

  private extractData(res: Response): string {
    return res.json(); //res.json().data as string
  }

  private extractText(res: Response): string {
    return res.text();
  }

  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // TODO: better job of transforming error for user consumption
      console.log(`${operation} failed: ${error.message}`);

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }
}
