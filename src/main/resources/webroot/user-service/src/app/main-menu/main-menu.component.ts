import { Component, OnInit, Input, Output, EventEmitter, OnDestroy } from '@angular/core';

import { ChatManagementService } from '../chat-management.service';
import { UserSessionService } from '../user-session.service';
import { Subscription, timer } from 'rxjs';

@Component({
  selector: 'app-main-menu',
  templateUrl: './main-menu.component.html',
  styleUrls: ['./main-menu.component.css']
})
export class MainMenuComponent implements OnInit, OnDestroy {

  @Output() notifyLogout = new EventEmitter<boolean>();
  private timer;
  private timerSub: Subscription;
  private initialTimerDelay = 0;
  private refreshInterval = 10000;
  username: string;
  availableChats;
  yourChats;
  chatName: string;

  constructor(
    private chatService: ChatManagementService,
    private userSessionService: UserSessionService,
  ) { }

  ngOnInit() {
    this.username = this.userSessionService.getUsername();
    this.timer = timer(this.initialTimerDelay, this.refreshInterval);
    this.timerSub = this.timer.subscribe(() => {
      this.getAvailableChats();
    });
    this.getUserChats();
  }

  getChats() {
    this.getAvailableChats();
    this.getUserChats();
  }

  getAvailableChats() {
    // console.log('getting available chats');
    this.chatService.getAvailableChat(this.userSessionService.getUsername())
      .subscribe(returnedAvailableChats => {
        this.availableChats = returnedAvailableChats;
        // console.log(this.availableChats);
      });
  }

  getUserChats() {
    this.chatService.getUserChat(this.userSessionService.getUsername())
      .subscribe(returnedUserChats => {
        this.yourChats = returnedUserChats;
        // console.log(this.yourChats);
      });
  }

  logout() {
    this.userSessionService.logout().subscribe(response => {
      // console.log('back to login menu');
      this.notifyLogout.emit(true);
    });
  }

  createChat() {
    if (this.chatName == null || this.chatName == '') {
      console.log("Can't create a chat with no name");
    } else {
      let newChatData = {
        chatname: this.chatName,
        creator: this.username
      }
      // console.log(newChatData);
      this.chatName = '';
      this.chatService.createChat(newChatData)
        .subscribe(response => {
          // console.log(response);
          this.getAvailableChats();
        });
    }
  }

  ngOnDestroy() {
    this.timerSub.unsubscribe();
  }
}
