import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { YourChatsComponent } from './your-chats.component';

describe('YourChatsComponent', () => {
  let component: YourChatsComponent;
  let fixture: ComponentFixture<YourChatsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ YourChatsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(YourChatsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
