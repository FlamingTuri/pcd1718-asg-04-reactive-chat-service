export class Message {
  date: string;
  username: string;
  text: string;
}