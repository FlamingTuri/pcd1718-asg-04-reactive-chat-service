import { Component, OnInit, OnDestroy, Input, ViewChild } from '@angular/core';
import { ChatService } from './chat.service';
import { UserSessionService } from '../../../user-session.service';
import { Message } from './message';
import { BsModalComponent } from 'ng2-bs3-modal';

@Component({
  selector: 'app-chat',
  templateUrl: './chat.component.html',
  styleUrls: ['./chat.component.css'],
  providers: [ChatService]
})
export class ChatComponent implements OnInit, OnDestroy {

  @ViewChild('modal')
  private displayChatModal: BsModalComponent;

  @Input() chat;

  messages = [];
  connection;
  message: Message;
  userData;
  isInCS: boolean = false;

  constructor(
    private chatService: ChatService,
    private userSessionService: UserSessionService) { }

  ngOnInit() {
    this.chatService.setUrl(this.chat.address);
    this.chatService.getStoredMessages(this.chat)
      .subscribe(result => {
        // console.log(result);
        this.messages = result;
      });
    this.connection = this.chatService.getMessages().subscribe(messageType => {
      // console.log(message);
      // console.log(messageType);
      switch (messageType.type) {
        case 'message': {
          this.messages.push(messageType.content);
          break;
        }
        case 'acquired-cs': {
          if (messageType.content.username == this.message.username) {
            this.isInCS = true;
          }
          break;
        }
        case 'released-cs': {
          if (messageType.content.username == this.message.username) {
            this.isInCS = false;
          }
          break;
        }
        default: {
          break;
        }
      }
    });
    this.message = new Message();
    this.message.username = this.userSessionService.getUsername();
    this.userData = {
      username: this.message.username
    }
  }

  ngOnDestroy() {
    this.connection.unsubscribe();
  }

  openChat() {
    this.displayChatModal.open();
  }

  sendMessage() {
    if (this.message.text != '' && this.message.text != null) {
      // console.log(this.message);
      this.chatService.sendMessage(this.message);
    }
    this.message.text = '';
  }

  enterCricticalSection() {    
    this.chatService.requestCriticalSection(this.userData);
  }

  leaveCricticalSection() {
    this.chatService.leaveCriticalSection(this.userData);
  }
}
