import { Component, OnInit, Input, Output, EventEmitter, ViewChild, ViewChildren, QueryList } from '@angular/core';
import { ChatManagementService } from '../../chat-management.service';
import { UserSessionService } from '../../user-session.service';
import { ChatComponent } from './chat/chat.component';
import { BsModalComponent } from 'ng2-bs3-modal';

@Component({
  selector: 'app-your-chats',
  templateUrl: './your-chats.component.html',
  styleUrls: ['./your-chats.component.css']
})
export class YourChatsComponent implements OnInit {

  @ViewChildren(BsModalComponent)
  modals: QueryList<BsModalComponent>;
  @ViewChildren(ChatComponent)
  subscribedChats: QueryList<ChatComponent>;
  
  @Input() yourChats;
  @Output() refresh = new EventEmitter<any>();
  chatOpened;
  openedChatMessages;

  constructor(
    private chatService: ChatManagementService,
    private userSessionService: UserSessionService) { }

  ngOnInit() {
  }

  openChat(i) {
    // this.modals.toArray()[i].open();
    this.subscribedChats.toArray()[i].openChat();
  }

  leaveChat(chat) {
    this.chatService.leaveChat(chat, this.userSessionService.getUsername())
      .subscribe(result => {
        // console.log('emit refresh');
        this.refresh.emit();
      });
  }
}
