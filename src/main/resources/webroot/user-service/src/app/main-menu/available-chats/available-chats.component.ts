import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { ChatManagementService } from '../../chat-management.service';
import { UserSessionService } from '../../user-session.service';

@Component({
  selector: 'app-available-chats',
  templateUrl: './available-chats.component.html',
  styleUrls: ['./available-chats.component.css']
})
export class AvailableChatsComponent implements OnInit {

  @Input() availableChats;
  @Output() refresh = new EventEmitter<any>();

  constructor(
    private chatService: ChatManagementService,
    private userSessionService: UserSessionService) { }

  ngOnInit() {
  }

  subscribeToChat(chat) {
    this.chatService.subscribeToChat(chat, this.userSessionService.getUsername())
      .subscribe(result => {
        console.log('emit refresh')
        this.refresh.emit();
      });
  }

}
