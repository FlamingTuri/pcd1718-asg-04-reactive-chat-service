import { Injectable, OnInit } from '@angular/core';
import { Subject, Observable, of } from 'rxjs';
import * as io from 'socket.io-client';
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { tap, catchError } from 'rxjs/operators';
import { ReactiveChatServiceApiService } from '../../../reactive-chat-service-api.service';
import { MessageType } from './message-type';

@Injectable({
  providedIn: 'root'
})
export class ChatService implements OnInit {

  private url;
  private socket;
  private headers = new HttpHeaders({ 'Content-Type': 'application/json' });
  private options: { headers, responseType: 'json' };
  private messageType = new MessageType();

  constructor(
    private http: HttpClient,
    private reactiveChatServiceApi: ReactiveChatServiceApiService) {  }

  ngOnInit() {

  }

  setUrl(port) {
    this.url = 'http://localhost:' + port;
    this.socket = io(this.url);
  }

  getStoredMessages(chat): Observable<any> {
    let url = this.reactiveChatServiceApi.getServerPath(this.reactiveChatServiceApi.getChatMessagesApi);
    return this.http.post(url, chat, this.options).pipe(
      tap((response: Response) => {
        // console.log(response);
        return response;
      }),
      catchError(this.handleError<any>('getChatMessages', "error"))
    );
  }

  sendMessage(message) {
    this.socket.emit('add-message', message);
  }

  getMessages() {
    let observable = new Observable<MessageType>(observer => {
      this.socket.on('message', (data) => {
        this.messageType.type = 'message';
        this.messageType.content = data;
        observer.next(this.messageType);
      });
      this.socket.on('acquired-cs', (data) => {
        this.messageType.type = 'acquired-cs';
        this.messageType.content = data;
        observer.next(this.messageType);
      });
      this.socket.on('released-cs', (data) => {
        this.messageType.type = 'released-cs';
        this.messageType.content = data;
        observer.next(this.messageType);
      });
      return () => {
        this.socket.disconnect();
      };
    })
    return observable;
  }

  requestCriticalSection(userData) {
    this.socket.emit('enter-cs-message', userData);
  }

  leaveCriticalSection(userData) {
    this.socket.emit('leave-cs-message', userData);
  }

  getCriticalSectionResult() {
    let observable = new Observable(observer => {

      return () => {
        this.socket.disconnect();
      };
    })
    return observable;
  }

  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // TODO: better job of transforming error for user consumption
      console.log(`${operation} failed: ${error.message}`);

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }
}
