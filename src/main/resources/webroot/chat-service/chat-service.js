'use strict';

let portscanner = require('portscanner'); // to select available port
let app = require('express')();
let httpServer = require('http').Server(app);
let io = require('socket.io')(httpServer); // to create Socket.io sockets
let httpClient = require('http');
let mysql = require('mysql'); // to connect to mysql database
var fs = require('fs'); // to access the filesystem

var chatname = process.argv[2];


var config = JSON.parse(fs.readFileSync('../../config/supervisorNumber.json', 'utf8'));
var supervisorNumber = config.number;

var minPortNumber = 8081 + supervisorNumber;
var maxPortNumber = 65535;

var timeout;
var userInCS = null;
var maxTimeInCS = 10000;

function prettyFormat(string) {
  return '[' + string + ']';
}

portscanner.findAPortNotInUse(minPortNumber, maxPortNumber, '127.0.0.1', function (error, port) {
  // console.log('AVAILABLE PORT AT: ' + port)

  if (error) console.err(error); // throw err;

  // connecting to MySQL database
  var con = mysql.createConnection({
    host: "localhost",
    user: "username",
    password: "password",
    database: "ReactiveChatService"
  });

  con.connect(function (err) {
    if (err) {
      console.err(err); // throw err;
    } else {
      console.log("Connected to DB");
    }
  });

  // intercepts http requests
  app.get('/isAlive', function (req, res) {
    var chatnameToJson = '{ "chatname": "' + chatname + '" }';
    res.end(chatnameToJson);
  });

  // creating WebSocket for chat message exchange
  io.on('connection', (socket) => {
    console.log('user connected');

    // log user disconnect activity
    socket.on('disconnect', function () {
      console.log('user disconnected');
    });

    // distribute incoming messages to chat users
    socket.on('add-message', (message) => {
      if (userInCS == null || userInCS == message.username) {
        console.log('received message: ');
        console.log(message);

        var date = new Date().toJSON().slice(0, 19).replace('T', ' ');
        console.log(date);

        var sql = "INSERT INTO `Messages` (`date`, `text`, `user`, `chat`) ";
        sql += "SELECT '" + date + "', '" + message.text + "', User.id, Chat.id ";
        sql += "FROM `User`, `Chat` ";
        sql += "WHERE User.username LIKE '" + message.username + "' ";
        sql += "AND Chat.name = '" + chatname + "' ";

        con.query(sql, function (err, result) {
          if (err) {
            console.err(err); // throw err;
          } else {
            message.date = date;
            io.emit('message', message);
          }
        });
      } else {
        console.warn(prettyFormat(message.username) + ' message rejected: ' + prettyFormat(userInCS) + ' is in critical section');
      }
    });

    // handle enter critical section request
    socket.on('enter-cs-message', (message) => {
      if (userInCS == null) {
        userInCS = message.username;
        var debugMsg = prettyFormat(userInCS) + ' forcefully left critical section';
        timeout = setTimeout(resetUserInCS, maxTimeInCS, debugMsg);
        io.emit('acquired-cs', message);
        console.log(prettyFormat(userInCS) + ' acquired critical section');
      } else {
        io.emit('refused-cs-acquire', message);
        let warningMsg = 'user ' + prettyFormat(message.username) + ' can\'t enter in critical section: ';
        warningMsg += 'user ' + prettyFormat(userInCS) + ' is in critical section';
        console.warn(warningMsg);
      }
    });

    // handle leave critical section message
    socket.on('leave-cs-message', (message) => {
      if (userInCS == message.username) {
        clearTimeout(timeout);
        resetUserInCS(prettyFormat(userInCS) + ' left critical section');
      } else {
        console.warn('user ' + prettyFormat(message.username) + ' does not own critical section -> he can\'t leave it!');
        io.emit('refused-cs-release', message);
      }
    });

    function resetUserInCS(debugMsg) {
      let message = {
        username: userInCS
      }
      io.emit('released-cs', message);
      userInCS = null;
      console.log(debugMsg);
    }
  });

  httpServer.listen(port, () => {
    // updates the db with current chat address
    var sql = "UPDATE `Chat` ";
    sql += "SET `address` = '" + port + "' ";
    sql += "WHERE `Chat`.`name` LIKE '" + chatname + "' ";

    con.query(sql, function (err, result) {
      if (err) console.err(err); // throw err;
    });
    console.log('Started chat ' + prettyFormat(chatname) + ' on port ' + port);

    // notify register that chat is ready
    var options = {
      hostname: 'localhost',
      port: 8080,
      path: '/chat/ready',
      agent: false  // create a new agent just for this one request
    };

    httpClient.get(options, function (res) {
      console.log(res.statusMessage);
    }).on("error", (err) => {
      console.log("Error: " + err.message);
    });

    var monitorChatUserNumber = function () {
      var sql = "SELECT COUNT(*) AS ParticipantNumber ";
      sql += "FROM ChatParticipant, Chat ";
      sql += "WHERE Chat.name LIKE '" + chatname + "' ";
      sql += "AND ChatParticipant.chat = Chat.id ";

      con.query(sql, function (err, result) {
        if (err) console.err(err); // throw err;

        // accessing to returned RawResult
        var participantNumber = result[0].ParticipantNumber
        // if no user is subscribed to the chat the process 
        // can be closed freeing up system resources
        if (participantNumber <= 0) {
          console.log('No users are subscribed to chat ' + prettyFormat(chatname));
          console.log('Quitting...');
          process.exit();
        }
      });
    };
    var intervalID = setInterval(monitorChatUserNumber, 10000);

  });

});
