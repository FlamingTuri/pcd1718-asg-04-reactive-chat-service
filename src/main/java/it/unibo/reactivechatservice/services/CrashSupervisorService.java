package it.unibo.reactivechatservice.services;

import java.io.File;
import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.AsyncResult;
import io.vertx.core.Future;
import io.vertx.core.buffer.Buffer;
import io.vertx.core.http.HttpMethod;
import io.vertx.core.http.HttpServer;
import io.vertx.core.http.HttpServerResponse;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.RoutingContext;
import io.vertx.ext.web.client.HttpRequest;
import io.vertx.ext.web.client.HttpResponse;
import io.vertx.ext.web.client.WebClient;
import io.vertx.ext.web.handler.BodyHandler;
import io.vertx.ext.web.handler.CorsHandler;
import it.unibo.reactivechatservice.utils.ConfigReader;
import it.unibo.reactivechatservice.utils.AppValues;

public class CrashSupervisorService extends AbstractVerticle {

    private final int supervisorPort;
    private int supervisorsNumber;
    private WebClient webClient;

    public CrashSupervisorService(int portNumber) {
        supervisorPort = portNumber;
    }

    @Override
    public void start() throws Exception {
        Router router = Router.router(vertx);

        router.route().handler(BodyHandler.create());

        Set<String> allowedHeaders = new HashSet<>();
        allowedHeaders.add("x-requested-with");
        allowedHeaders.add("Access-Control-Allow-Origin");
        allowedHeaders.add("origin");
        allowedHeaders.add("Content-Type");
        allowedHeaders.add("accept");
        allowedHeaders.add("X-PINGARUNER");

        Set<HttpMethod> allowedMethods = new HashSet<>();
        allowedMethods.add(HttpMethod.GET);
        allowedMethods.add(HttpMethod.POST);
        allowedMethods.add(HttpMethod.OPTIONS);
        allowedMethods.add(HttpMethod.DELETE);
        allowedMethods.add(HttpMethod.PATCH);
        allowedMethods.add(HttpMethod.PUT);

        CorsHandler corsHandler = CorsHandler.create("*");
        corsHandler.allowedHeaders(allowedHeaders);
        corsHandler.allowedMethods(allowedMethods);
        router.route().handler(corsHandler);

        // REST API
        router.get(AppValues.IS_ALIVE_REST_API).handler(this::isAlive);

        HttpServer server = vertx.createHttpServer();
        server.requestHandler(router::accept);
        server.listen(supervisorPort, listenHandler -> {
            if (listenHandler.succeeded()) {
                log(CrashSupervisorService.class.getSimpleName() + " running on port " + supervisorPort);
            } else {
                log("Error in service start");
                log("" + listenHandler.cause());
                vertx.close();
            }
        });

        webClient = WebClient.create(vertx);
        vertx.executeBlocking(future -> {
            supervisorsNumber = ConfigReader.getSupervisorNumber();
            future.complete();
        }, result -> {
            log("supervisors number " + supervisorsNumber);
            monitorLiveliness();
        });
    }

    private void isAlive(RoutingContext routingContext) {
        JsonObject supervisorData = new JsonObject();
        supervisorData.put("class", CrashSupervisorService.class.getName());
        supervisorData.put("address", supervisorPort);

        HttpServerResponse response = routingContext.response();
        response.putHeader("Content-Type", "application/text");
        response.end(supervisorData.encodePrettily());
    }

    private void monitorLiveliness() {
        long timerID = vertx.setPeriodic(AppValues.CRASH_MONITOR_INTERVAL, id -> {
            checkCoreServiceLiveliness();
            checkSupervisorsLiveliness();
        });
    }

    private void checkCoreServiceLiveliness() {
        HttpRequest<Buffer> httpRequest = webClient.get(AppValues.CORE_PORT_NUMBER, AppValues.HOST, AppValues.IS_ALIVE_REST_API);
        httpRequest.timeout(AppValues.RESPONSE_TIMEOUT).send(ar -> {
            String expectedClassName = CoreService.class.getName();
            String errorMessage = "relaunch core service";
            String command = AppValues.RUN_CORE_SERVICE;
            onResult(ar, expectedClassName, errorMessage, command);
        });
    }

    private void checkSupervisorsLiveliness() {
        int start = AppValues.CORE_PORT_NUMBER + 1;
        int stop = start + supervisorsNumber;
        for (int port = start; port < stop; port++) {
            if (port != supervisorPort) {
                HttpRequest<Buffer> httpRequest = webClient.get(port, AppValues.HOST, AppValues.IS_ALIVE_REST_API);
                httpRequest.timeout(AppValues.RESPONSE_TIMEOUT).send(ar -> {
                    String expectedClassName = CrashSupervisorService.class.getName();
                    String errorMessage = "relaunch supervisor";
                    String command = AppValues.RUN_CRASH_SUPERVISOR_SERVICE;
                    onResult(ar, expectedClassName, errorMessage, command);
                });
            }
        }
    }

    private void onResult(AsyncResult<HttpResponse<Buffer>> ar, String expectedClassName, String errorMessage,
            String command) {
        boolean relaunch = true;
        String portAddress = "";

        if (ar.succeeded()) {
            HttpResponse<Buffer> result = ar.result();
            try {
                JsonObject body = result.bodyAsJsonObject();
                // log(body.encodePrettily());
                String className = body.getString("class");
                portAddress = "" + body.getInteger("address");
                if (className.equals(expectedClassName)) {
                    relaunch = false;
                } else {
                    // port is used by something else
                    relaunch = false;
                    log("port " + portAddress + " used by something else");
                }
            } catch (Exception e) {
                // port is used by something else
                relaunch = false;
                log("can't get port address");
                e.printStackTrace();
            }
        } else {
            String errorMsg = ar.cause().getMessage();
            portAddress = errorMsg.substring(errorMsg.length() - 4);
        }
        if (relaunch) {
            errorMessage += " on port " + portAddress;
            log(errorMessage);
            String[] cmdarray = { command, portAddress };
            restartService(cmdarray);
        }
    }

    private void restartService(String[] cmdarray) {
        Future<Void> f = Future.future();
        vertx.executeBlocking(future -> {
            try {
                Runtime runTime = Runtime.getRuntime();
                runTime.exec(cmdarray, null, new File(AppValues.SCRIPT_PATH));
            } catch (IOException e) {
                e.printStackTrace();
            }
            future.complete();
        }, f.completer());
    }

    private void log(String string) {
        System.out.println(string + "\n");
    }
}
