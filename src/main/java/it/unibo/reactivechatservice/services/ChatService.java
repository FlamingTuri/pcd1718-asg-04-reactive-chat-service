package it.unibo.reactivechatservice.services;

import java.text.DateFormat;
import java.time.Instant;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.eventbus.EventBus;
import io.vertx.core.http.HttpMethod;
import io.vertx.core.http.HttpServerResponse;
import io.vertx.core.json.Json;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.bridge.PermittedOptions;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.RoutingContext;
import io.vertx.ext.web.handler.CorsHandler;
import io.vertx.ext.web.handler.StaticHandler;
import io.vertx.ext.web.handler.sockjs.BridgeOptions;
import io.vertx.ext.web.handler.sockjs.SockJSHandler;
import it.unibo.reactivechatservice.utils.CheckAvailablePorts;

/**
 * Not working, java does not support socket.io
 */
public class ChatService extends AbstractVerticle {

    private final CheckAvailablePorts checkAvailablePorts = new CheckAvailablePorts();

    @Override
    public void start() {
        Router router = Router.router(vertx);

        Set<String> allowedHeaders = new HashSet<>();
        allowedHeaders.add("x-requested-with");
        allowedHeaders.add("Access-Control-Allow-Origin");
        allowedHeaders.add("origin");
        allowedHeaders.add("Content-Type");
        allowedHeaders.add("accept");
        allowedHeaders.add("X-PINGARUNER");

        Set<HttpMethod> allowedMethods = new HashSet<>();
        allowedMethods.add(HttpMethod.GET);
        allowedMethods.add(HttpMethod.POST);
        allowedMethods.add(HttpMethod.OPTIONS);
        allowedMethods.add(HttpMethod.DELETE);
        allowedMethods.add(HttpMethod.PATCH);
        allowedMethods.add(HttpMethod.PUT);

        router.route().handler(CorsHandler.create("*").allowedHeaders(allowedHeaders).allowedMethods(allowedMethods));

        router.post("/message/:messageContent").handler(this::message);

        // Serve the static pages
        router.route().handler(StaticHandler.create());

        // Socket configuration
        PermittedOptions inboundPermitted1 = new PermittedOptions().setAddress("chat.to.server");
        PermittedOptions outboundPermitted1 = new PermittedOptions().setAddress("chat.to.client");
        BridgeOptions bridgeOptions = new BridgeOptions().addInboundPermitted(inboundPermitted1)
                .addOutboundPermitted(outboundPermitted1);

        // Create the event bus bridge and add it to the router.
        SockJSHandler ebHandler = SockJSHandler.create(vertx).bridge(bridgeOptions);
        // router.route("/eventbus/*").handler(ebHandler);
        router.route("/*").handler(ebHandler);
        try {
            String chatname = System.getProperty("name");
            int portNumber = checkAvailablePorts.getFirstAvailablePort();
            vertx.createHttpServer().requestHandler(router::accept).listen(portNumber);
            System.out.println("Chat service " + chatname + " running on port " + portNumber);
        } catch (Exception e) {
            e.printStackTrace();
            System.exit(-1);
        }

        EventBus eb = vertx.eventBus();

        // Register to listen for messages coming IN to the server
        eb.consumer("chat.to.server").handler(message -> {
            // Create a timestamp string
            String timestamp = DateFormat.getDateTimeInstance(DateFormat.SHORT, DateFormat.MEDIUM)
                    .format(Date.from(Instant.now()));
            // Send the message back out to all clients with the timestamp prepended.
            eb.publish("chat.to.client", timestamp + ": " + message.body());
        });
    }

    private void message(RoutingContext routingContext) {
        System.out.println(routingContext.getBodyAsJson());
        System.out.println(routingContext.getBodyAsString());
        String messageContent = routingContext.request().getParam("messageContent");
        System.out.println(messageContent);

        JsonObject jsonMessageContent = new JsonObject(messageContent);
        String senderUsername = (String) jsonMessageContent.getValue("username");
        String message = (String) jsonMessageContent.getValue("message");
        HttpServerResponse response = routingContext.response();

        response.putHeader("Content-Type", "application/text").end(Json.encodePrettily(senderUsername));
    }
}
