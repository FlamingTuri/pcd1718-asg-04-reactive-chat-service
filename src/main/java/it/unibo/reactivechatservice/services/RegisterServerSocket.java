package it.unibo.reactivechatservice.services;

import com.corundumstudio.socketio.AckRequest;
import com.corundumstudio.socketio.Configuration;
import com.corundumstudio.socketio.SocketIOClient;
import com.corundumstudio.socketio.SocketIOServer;
import com.corundumstudio.socketio.listener.DataListener;

import it.unibo.reactivechatservice.utils.ChatObject;
import it.unibo.reactivechatservice.utils.CheckAvailablePorts;

public class RegisterServerSocket {
    private static final String HOST_NAME = "localhost";
    private final int portNumber;
    private final SocketIOServer server;

    public RegisterServerSocket() {
        CheckAvailablePorts checkAvailablePorts = new CheckAvailablePorts();
        portNumber = checkAvailablePorts.getFirstAvailablePort();

        Configuration config = new Configuration();
        config.setHostname(HOST_NAME);
        config.setPort(portNumber);

        server = new SocketIOServer(config);
        server.addEventListener("chatevent", ChatObject.class, messageHandler());
    }

    private DataListener<ChatObject> messageHandler() {
        DataListener<ChatObject> dataListener = (SocketIOClient client, ChatObject data, AckRequest ackRequest) -> {
            // TypeScript socket.io sends the same client for applications running on
            // different ports. Only one client receives the message back
            System.out.println(data.getUsername());
            System.out.println(client);
            System.out.println(client.getRemoteAddress());
            System.out.println("connected clients:");
            server.getAllClients().forEach(c -> {
                System.out.println(c);
            });
            // broadcast messages to all clients
            server.getBroadcastOperations().sendEvent("chatevent", data);
        };
        return dataListener;
    }

    public void startServer() {
        try {
            server.start();
        } catch (Exception e) {
            stopServer();
        }
    }

    public void stopServer() {
        server.stop();
    }

    public int getPortNumber() {
        return portNumber;
    }
}
