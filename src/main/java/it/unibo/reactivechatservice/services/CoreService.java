package it.unibo.reactivechatservice.services;

import java.io.File;
import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.AsyncResult;
import io.vertx.core.Future;
import io.vertx.core.Handler;
import io.vertx.core.buffer.Buffer;
import io.vertx.core.http.HttpMethod;
import io.vertx.core.http.HttpServer;
import io.vertx.core.http.HttpServerResponse;
import io.vertx.core.json.Json;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.jdbc.JDBCClient;
import io.vertx.ext.sql.ResultSet;
import io.vertx.ext.sql.SQLConnection;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.RoutingContext;
import io.vertx.ext.web.client.HttpRequest;
import io.vertx.ext.web.client.HttpResponse;
import io.vertx.ext.web.client.WebClient;
import io.vertx.ext.web.handler.BodyHandler;
import io.vertx.ext.web.handler.CorsHandler;
import it.unibo.reactivechatservice.utils.ConfigReader;
import it.unibo.reactivechatservice.utils.Logger;
import it.unibo.reactivechatservice.utils.QueryFor;
import it.unibo.reactivechatservice.utils.QueryUtils;
import it.unibo.reactivechatservice.utils.ResponseHandler;
import it.unibo.reactivechatservice.utils.AppValues;

public class CoreService extends AbstractVerticle {

    private final QueryFor queryFor = new QueryFor();
    private final Set<JsonObject> chatToCreateQueue = new HashSet<>();
    private JDBCClient jdbcClient;
    private WebClient webClient;
    private String sql;

    @Override
    public void start() throws Exception {
        Router router = Router.router(vertx);

        router.route().handler(BodyHandler.create());

        Set<String> allowedHeaders = new HashSet<>();
        allowedHeaders.add("x-requested-with");
        allowedHeaders.add("Access-Control-Allow-Origin");
        allowedHeaders.add("origin");
        allowedHeaders.add("Content-Type");
        allowedHeaders.add("accept");
        allowedHeaders.add("X-PINGARUNER");

        Set<HttpMethod> allowedMethods = new HashSet<>();
        allowedMethods.add(HttpMethod.GET);
        allowedMethods.add(HttpMethod.POST);
        allowedMethods.add(HttpMethod.OPTIONS);
        allowedMethods.add(HttpMethod.DELETE);
        allowedMethods.add(HttpMethod.PATCH);
        allowedMethods.add(HttpMethod.PUT);

        CorsHandler corsHandler = CorsHandler.create("*");
        corsHandler.allowedHeaders(allowedHeaders);
        corsHandler.allowedMethods(allowedMethods);
        router.route().handler(corsHandler);

        // REST API
        router.get(AppValues.IS_ALIVE_REST_API).handler(this::isAlive);
        router.get(AppValues.CHAT_READY_REST_API).handler(this::checkChatsStatus);

        router.post("/user/register").handler(this::register);
        router.post("/user/login").handler(this::login);
        router.post("/user/logout").handler(this::logout);

        router.post("/chats/availableList").handler(this::availableChats);
        router.post("/chats/subscribedList").handler(this::userSubscribedChats);

        router.post("/chat/create").handler(this::createChat);
        router.post("/chat/enter").handler(this::enterChat);
        router.post("/chat/leave").handler(this::leaveChat);
        router.post("/chat/messages").handler(this::getChatStoredMessages);

        vertx.executeBlocking(future -> {
            JsonObject properties = ConfigReader.getJsonMysSqlConfig();
            future.complete(properties);
        }, result -> {
            JsonObject properties = (JsonObject) result.result();
            jdbcClient = JDBCClient.createShared(vertx, properties);
            Logger.log("Connected to db successfully");

            HttpServer server = vertx.createHttpServer();
            server.requestHandler(router::accept);
            server.listen(AppValues.CORE_PORT_NUMBER, listenHandler -> {
                if (listenHandler.succeeded()) {
                    Logger.log(CoreService.class.getSimpleName() + " running on port " + AppValues.CORE_PORT_NUMBER);
                } else {
                    Logger.log("Error in service start");
                    Logger.log("" + listenHandler.cause());
                    vertx.close();
                }
            });

            webClient = WebClient.create(vertx);
            getAliveChatList();
            monitorChatLiveliness();
        });
    }

    private void selfNotifyChatReady() {
        webClient.get(AppValues.CORE_PORT_NUMBER, AppValues.HOST, AppValues.CHAT_READY_REST_API).send(ar -> {
        });
    }

    private void monitorChatLiveliness() {
        long timerID = vertx.setPeriodic(AppValues.CHAT_LIVELINESS_MONITOR_INTERVAL, id -> {
            getAliveChatList();
            Logger.log("Chat queue size: " + chatToCreateQueue.size());
        });
    }

    private void getAliveChatList() {
        Handler<AsyncResult<ResultSet>> resultHandler = queryResult -> {
            if (queryResult.succeeded()) {
                JsonObject rs = queryResult.result().toJson();
                JsonArray responseJsonArray = rs.getJsonArray("rows");
                Logger.log(responseJsonArray.encodePrettily());
                for (int i = 0; i < responseJsonArray.size(); i++) {
                    chatToCreateQueue.add(responseJsonArray.getJsonObject(i));
                }
                // send self message to start chat creation
                selfNotifyChatReady();
            } else {
                Logger.logQueryFailed(queryResult);
            }
        };

        Handler<AsyncResult<SQLConnection>> handler = res -> {
            if (res.succeeded()) {
                SQLConnection connection = res.result();

                sql = queryFor.selectActiveChats();
                connection.query(sql, resultHandler);

                connection.close();
            } else {
                Logger.dbConnectionFailMsg();
            }
        };
        jdbcClient.getConnection(handler);
    }

    private void runNewChatServiceIstance(String chatname) {
        Future<Void> f = Future.future();
        vertx.executeBlocking(future -> {
            try {
                Runtime runTime = Runtime.getRuntime();
                Logger.log("creating chat " + prettyFormat(chatname));
                String[] cmdarray = { AppValues.RUN_CHAT_SERVICE, chatname };
                runTime.exec(cmdarray, null, new File(AppValues.SCRIPT_PATH));
            } catch (IOException e) {
                e.printStackTrace();
            }
            future.complete();
        }, f.completer());
    }

    private String prettyFormat(Object param) {
        return "[" + param + "]";
    }

    private void isAlive(RoutingContext routingContext) {
        JsonObject coreServiceData = new JsonObject();
        coreServiceData.put("class", CoreService.class.getName());
        coreServiceData.put("address", AppValues.CORE_PORT_NUMBER);

        HttpServerResponse response = routingContext.response();
        response.putHeader("Content-Type", "application/text");
        response.end(coreServiceData.encodePrettily());
    }

    private void checkChatsStatus(RoutingContext routingContext) {
        ResponseHandler.sendResponse(routingContext.response(), "");

        if (!chatToCreateQueue.isEmpty()) {
            JsonObject currentChat = chatToCreateQueue.stream().findFirst().get();
            String chatname = currentChat.getString("name");
            chatToCreateQueue.remove(currentChat);
            if (currentChat.getString("address") == null) {
                runNewChatServiceIstance(chatname);
            } else {
                int address = Integer.parseInt(currentChat.getString("address"));
                HttpRequest<Buffer> httpRequest = webClient.get(address, AppValues.HOST, AppValues.IS_ALIVE_REST_API);
                httpRequest.timeout(AppValues.RESPONSE_TIMEOUT);
                httpRequest.send(ar -> {
                    handleLivelinessResponse(ar, chatname);
                });
            }
        }
    }

    private void handleLivelinessResponse(AsyncResult<HttpResponse<Buffer>> ar, String chatname) {
        boolean launchNewChatInstance = true;
        String message;

        if (ar.succeeded()) {
            HttpResponse<Buffer> response = ar.result();

            try {
                String responseChatname = response.bodyAsJsonObject().getString("chatname");
                // log(responseChatname);
                if (responseChatname.equals(chatname)) {
                    launchNewChatInstance = false;
                    // received response from chat -> should not create a new one
                    message = "Chat " + prettyFormat(chatname) + " is alive";
                    // make a self request to process next chat in the queue
                    selfNotifyChatReady();
                } else {
                    // received response, but it is from another active chat,
                    // the current address on the server is outdated and
                    // a new chat must be created
                    message = "Received response by chat " + prettyFormat(responseChatname);
                    message += ", but the requested one was " + prettyFormat(chatname);
                }
            } catch (Exception e) {
                message = "Unexpected response";
            }
        } else {
            // chat is not alive, it should be created
            message = "Response timeout for chat " + prettyFormat(chatname);
            message += ", reason: " + ar.cause().getMessage();
        }

        Logger.log(message);
        if (launchNewChatInstance) {
            runNewChatServiceIstance(chatname);
        }
    }

    private void register(RoutingContext routingContext) {
        JsonObject body = routingContext.getBodyAsJson();
        Logger.log(body.encodePrettily());
        String username = body.getString("username");
        String email = body.getString("email");
        String password = body.getString("password");
        String address = body.getString("address");

        HttpServerResponse response = routingContext.response();

        sql = queryFor.registration(username, email, password, address);
        Handler<AsyncResult<ResultSet>> resultHandler = queryResult -> {
            if (queryResult.succeeded()) {
                Logger.log("" + queryResult.result().toJson());
                ResponseHandler.sendResponse(response, Json.encodePrettily(username));
            } else {
                ResponseHandler.queryFailed(response, queryResult);
            }
        };
        QueryUtils.executeQuery(jdbcClient, sql, resultHandler, response);
    }

    private void login(RoutingContext routingContext) {
        JsonObject body = routingContext.getBodyAsJson();
        Logger.log(body.encodePrettily());
        String loginCredential = body.getString("loginCredential");
        String password = body.getString("password");
        // String address = body.getString("address");

        HttpServerResponse response = routingContext.response();

        sql = queryFor.login(loginCredential, password);
        Handler<AsyncResult<ResultSet>> resultHandler = queryResult -> {
            if (queryResult.succeeded()) {
                JsonObject rs = queryResult.result().toJson();
                Logger.log(rs.encodePrettily());
                // log(rs.fieldNames().toString());
                // log(rs.getJsonArray("rows").encode());
                if (rs.getInteger("numRows") == 1) {
                    JsonObject entry = rs.getJsonArray("rows").getJsonObject(0);
                    JsonObject responseJsonObject = new JsonObject();
                    responseJsonObject.put("username", entry.getString("username"));
                    responseJsonObject.put("email", entry.getString("email"));
                    ResponseHandler.sendResponse(response, Json.encodePrettily(responseJsonObject));
                } else {
                    String reason = "Wrong email or password";
                    Logger.log(reason);
                    ResponseHandler.sendError(400, response, reason);
                }
            } else {
                ResponseHandler.queryFailed(response, queryResult);
            }
        };
        QueryUtils.executeQuery(jdbcClient, sql, resultHandler, response);
    }

    private void logout(RoutingContext routingContext) {
        ResponseHandler.sendResponse(routingContext.response(), Json.encodePrettily("logout successful"));
    }

    private void availableChats(RoutingContext routingContext) {
        Logger.log("getting available chats");
        JsonObject body = routingContext.getBodyAsJson();
        Logger.log(body.encodePrettily());
        String username = body.getString("username");

        HttpServerResponse response = routingContext.response();
        sql = queryFor.getAvailableChats(username);
        Handler<AsyncResult<ResultSet>> resultHandler = queryResult -> {
            if (queryResult.succeeded()) {
                // logQuerySuccedeed()
                JsonObject rs = queryResult.result().toJson();
                Logger.log(queryResult.result().toJson().encodePrettily());
                String chats = rs.getJsonArray("rows").encodePrettily();
                ResponseHandler.sendResponse(response, chats);
            } else {
                ResponseHandler.queryFailed(response, queryResult);
            }
        };
        QueryUtils.executeQuery(jdbcClient, sql, resultHandler, response);
    }

    private void userSubscribedChats(RoutingContext routingContext) {
        Logger.log("getting user's chats");
        JsonObject body = routingContext.getBodyAsJson();
        Logger.log(body.encodePrettily());
        String username = body.getString("username");

        HttpServerResponse response = routingContext.response();
        sql = queryFor.getUserPartecipatingChats(username);
        Handler<AsyncResult<ResultSet>> resultHandler = queryResult -> {
            if (queryResult.succeeded()) {
                // logQuerySuccedeed()
                JsonObject rs = queryResult.result().toJson();
                Logger.log(queryResult.result().toJson().encodePrettily());
                // String chats = Json.encodePrettily(rs.getJsonArray("rows"));
                String chats = rs.getJsonArray("rows").encodePrettily();
                response.putHeader("Content-Type", "application/json").end(chats);
            } else {
                ResponseHandler.queryFailed(response, queryResult);
            }
        };
        QueryUtils.executeQuery(jdbcClient, sql, resultHandler, response);
    }

    private void createChat(RoutingContext routingContext) {
        JsonObject body = routingContext.getBodyAsJson();
        Logger.log(body.encodePrettily());
        String chatname = body.getString("chatname");
        String username = body.getString("creator");

        HttpServerResponse response = routingContext.response();

        sql = queryFor.createChat(chatname);
        Handler<AsyncResult<ResultSet>> resultHandler = queryResult -> {
            if (queryResult.succeeded()) {
                // logQuerySuccedeed()
                // System.out.println(queryResult.result());
                JsonObject newChat = new JsonObject();
                newChat.put("name", chatname);
                newChat.putNull("address");
                chatToCreateQueue.add(newChat);
                // if this is the only chat present in the queue,
                // send a self notify, otherwise the system will handle
                // its creation after checking the other chats in the queue
                if (chatToCreateQueue.size() == 1) {
                    selfNotifyChatReady();
                }
                ResponseHandler.sendResponse(response, Json.encodePrettily(username));
            } else {
                ResponseHandler.queryFailed(response, queryResult);
            }
        };
        QueryUtils.executeQuery(jdbcClient, sql, resultHandler, response);
    }

    private void enterChat(RoutingContext routingContext) {
        JsonObject body = routingContext.getBodyAsJson();
        Logger.log(body.encodePrettily());
        String chatname = body.getString("chatname");
        String username = body.getString("username");

        HttpServerResponse response = routingContext.response();

        Handler<AsyncResult<SQLConnection>> handler = res -> {
            if (res.succeeded()) {
                SQLConnection connection = res.result();

                // if the chat has 0 participants, run new chat instance
                sql = queryFor.getChatNumberOfParticipants(chatname);
                Handler<AsyncResult<ResultSet>> resultHandler1 = queryResult -> {
                    if (queryResult.succeeded()) {
                        JsonArray rows = queryResult.result().toJson().getJsonArray("rows");
                        if (rows.getJsonObject(0).getInteger("ParticipantNumber") == 0) {
                            JsonObject chatServerToLaunch = new JsonObject();
                            chatServerToLaunch.put("name", chatname);
                            chatServerToLaunch.putNull("address");
                            chatToCreateQueue.add(chatServerToLaunch);
                            if (chatToCreateQueue.size() == 1) {
                                selfNotifyChatReady();
                            }
                            // sendResponse(response, "successfully launched chat " + chatname);
                        }
                    } else {
                        ResponseHandler.queryFailed(response, queryResult);
                    }
                };
                connection.query(sql, resultHandler1);

                // add user to ChatParticipants
                sql = queryFor.subscribeToChat(username, chatname);
                Handler<AsyncResult<ResultSet>> resultHandler2 = queryResult -> {
                    if (queryResult.succeeded()) {
                        ResponseHandler.sendResponse(response, "subscribed to chat " + chatname);
                    } else {
                        ResponseHandler.queryFailed(response, queryResult);
                    }
                };
                connection.query(sql, resultHandler2);

                connection.close();
            } else {
                ResponseHandler.dbConnectionFailMsg(response);
            }
        };
        jdbcClient.getConnection(handler);
    }

    private void leaveChat(RoutingContext routingContext) {
        JsonObject body = routingContext.getBodyAsJson();
        Logger.log(body.encodePrettily());
        String chatname = body.getString("chatname");
        String username = body.getString("username");

        HttpServerResponse response = routingContext.response();
        sql = queryFor.leaveChat(username, chatname);
        Handler<AsyncResult<ResultSet>> resultHandler = queryResult -> {
            if (queryResult.succeeded()) {
                // logQuerySuccedeed()
                // log("" + queryResult.result());
                ResponseHandler.sendResponse(response, "left chat " + chatname);
            } else {
                ResponseHandler.queryFailed(response, queryResult);
            }
        };
        QueryUtils.executeQuery(jdbcClient, sql, resultHandler, response);
    }

    private void getChatStoredMessages(RoutingContext routingContext) {
        JsonObject body = routingContext.getBodyAsJson();
        // log(body.encodePrettily());
        String chatname = body.getString("name");
        // log("getting messages of chat " + chatname);

        HttpServerResponse response = routingContext.response();
        sql = queryFor.chatStoredMessages(chatname);
        Handler<AsyncResult<ResultSet>> resultHandler = queryResult -> {
            if (queryResult.succeeded()) {
                // Logger.logQuerySuccedeed();
                JsonObject rs = queryResult.result().toJson();
                Logger.log(queryResult.result().toJson().encodePrettily());
                String chats = Json.encodePrettily(rs.getJsonArray("rows"));
                response.putHeader("Content-Type", "application/json").end(chats);
            } else {
                ResponseHandler.queryFailed(response, queryResult);
            }
        };
        QueryUtils.executeQuery(jdbcClient, sql, resultHandler, response);
    }

}
