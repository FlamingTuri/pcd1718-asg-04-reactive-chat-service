package it.unibo.reactivechatservice.utils;

import java.io.Serializable;

public class ChatObject implements Serializable {

    private static final long serialVersionUID = 1L;
    private String username;
    private String message;

    public ChatObject() {
    }

    public ChatObject(String username, String message) {
        super();
        this.username = username;
        this.message = message;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

}
