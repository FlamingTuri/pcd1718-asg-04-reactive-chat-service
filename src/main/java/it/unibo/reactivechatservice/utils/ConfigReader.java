package it.unibo.reactivechatservice.utils;

import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import io.vertx.core.json.JsonObject;

public class ConfigReader {

    public static Properties getMysSqlConfig() {
        Properties properties = new Properties();
        try {
            properties.load(new FileInputStream(AppValues.CONFIG_PATH + "/" + AppValues.MYSQL_CONFIG_FILE));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return properties;
    }

    public static JsonObject getJsonMysSqlConfig() {
        JsonObject properties = new JsonObject();
        getMysSqlConfig().forEach((key, value) -> {
            properties.put((String) key, (String) value);
        });
        return properties;
    }

    public static int getSupervisorNumber() {
        int supervisorNumber = -1;
        JSONParser parser = new JSONParser();
        try {
            FileReader fileReader = new FileReader(AppValues.CONFIG_PATH + "/" + AppValues.SUPERVISOR_NUMBER_FILE);
            Object object = parser.parse(fileReader);
            JSONObject jsonObject = (JSONObject) object;
            supervisorNumber = Integer.parseInt(jsonObject.get("number").toString());
        } catch (IOException | ParseException e) {
            e.printStackTrace();
        }
        return supervisorNumber;
    }

    public static void main(String[] args) {

        Properties properties = ConfigReader.getMysSqlConfig();
        System.out.println(properties);

        JsonObject jsonProperties = ConfigReader.getJsonMysSqlConfig();
        System.out.println(jsonProperties);

        int supervisorNumber = ConfigReader.getSupervisorNumber();
        System.out.println(supervisorNumber);
    }
}
