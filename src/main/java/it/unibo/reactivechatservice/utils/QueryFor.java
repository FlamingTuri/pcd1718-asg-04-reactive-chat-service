package it.unibo.reactivechatservice.utils;

public class QueryFor {

    private String sqlQuery;

    public QueryFor() {
    }

    public String createDB() {
        sqlQuery = "CREATE DATABASE IF NOT EXISTS ReactiveChatService";
        return sqlQuery;
    }

    public String queryEncodeParam(Object param) {
        return "'" + param + "'";
    }

    public String createUserTable() {
        sqlQuery = "CREATE TABLE IF NOT EXISTS `ReactiveChatService`.`User` ( ";
        sqlQuery += "`id` INT NOT NULL , ";
        sqlQuery += "`username` TEXT NOT NULL , ";
        sqlQuery += "`email` TEXT NOT NULL , ";
        sqlQuery += "`password` TEXT NOT NULL , ";
        sqlQuery += "`address` TEXT NOT NULL , ";
        sqlQuery += "PRIMARY KEY (`id`), ";
        sqlQuery += "UNIQUE (`username`(16), `email`(32))) ";
        sqlQuery += "ENGINE = InnoDB;";
        return sqlQuery;
    }

    public String createChatTable() {
        sqlQuery = "CREATE TABLE IF NOT EXISTS `ReactiveChatService`.`Chat` ( ";
        sqlQuery += "`id` INT NOT NULL , ";
        sqlQuery += "`name` TEXT NOT NULL , ";
        sqlQuery += "PRIMARY KEY (`id`)) ";
        sqlQuery += "ENGINE = InnoDB;";
        return sqlQuery;
    }

    public String selectActiveChats() {
        sqlQuery = "SELECT DISTINCT Chat.name, Chat.address ";
        sqlQuery += "FROM `ChatParticipant`, `Chat` ";
        sqlQuery += "WHERE ChatParticipant.chat LIKE Chat.id";
        return sqlQuery;
    }

    public String registration(String username, String email, String password, String address) {
        sqlQuery = "INSERT INTO `User` ";
        sqlQuery += "(`username`, `email`, `password`, `address`) ";
        sqlQuery += "VALUES ('" + username + "', '" + email + "', '" + password + "', '" + address + "')";
        return sqlQuery;
    }

    public String login(String loginCredential, String password) {
        sqlQuery = "SELECT * FROM `User` ";
        sqlQuery += "WHERE `username` LIKE '" + loginCredential + "' ";
        sqlQuery += "AND `password` LIKE '" + password + "' ";
        sqlQuery += "OR `email` LIKE '" + loginCredential + "' ";
        sqlQuery += "AND `password` LIKE '" + password + "'";
        // executeQuery(sqlQuery, queryResultHandler);
        return sqlQuery;
    }

    public String createChat(String chatname) {
        sqlQuery = "INSERT INTO `Chat` (`name`) ";
        sqlQuery += "VALUES ('" + chatname + "')";
        return sqlQuery;
    }

    public String getChatNumberOfParticipants(String chatname) {
        sqlQuery = "SELECT COUNT(*) AS ParticipantNumber ";
        sqlQuery += "FROM ChatParticipant, Chat ";
        sqlQuery += "WHERE Chat.name LIKE '" + chatname + "' ";
        sqlQuery += "AND ChatParticipant.chat = Chat.id ";
        return sqlQuery;
    }

    public String subscribeToChat(String username, String chatname) {
        sqlQuery = "INSERT INTO `ChatParticipant` (`user`, `chat`) ";
        sqlQuery += "SELECT User.id, Chat.id ";
        sqlQuery += "FROM `User`, `Chat` ";
        sqlQuery += "WHERE User.username LIKE '" + username + "' ";
        sqlQuery += "AND Chat.name = '" + chatname + "'";
        return sqlQuery;
    }

    public String leaveChat(String username, String chatname) {
        sqlQuery = "DELETE CP.* ";
        sqlQuery += "FROM `ChatParticipant` AS `CP`, `User` AS `U`, `Chat` AS `C` ";
        sqlQuery += "WHERE C.name LIKE '" + chatname + "' ";
        sqlQuery += "AND U.username LIKE '" + username + "' ";
        sqlQuery += "AND CP.user = U.id ";
        sqlQuery += "AND CP.chat = C.id ";
        return sqlQuery;
    }

    public String getAvailableChats(String username) {
        sqlQuery = "SELECT C.id, C.name ";
        sqlQuery += "FROM `Chat` AS `C` ";
        sqlQuery += "WHERE C.name NOT IN (SELECT C.name ";
        sqlQuery += "FROM `User` AS `U`, `ChatParticipant` AS `CP`, `Chat` AS `C` ";
        sqlQuery += "WHERE U.username LIKE '" + username + "' ";
        sqlQuery += "AND CP.user = U.id AND CP.chat = C.id)";
        return sqlQuery;
    }

    public String getUserPartecipatingChats(String username) {
        sqlQuery = "SELECT C.id, C.name, C.address ";
        sqlQuery += "FROM `User` AS `U`, `ChatParticipant` AS `CP`, `Chat` AS `C` ";
        sqlQuery += "WHERE U.username LIKE '" + username + "' ";
        sqlQuery += "AND CP.user = U.id AND CP.chat = C.id ";
        return sqlQuery;
    }

    public String chatStoredMessages(String chatname) {
        sqlQuery = "SELECT M.date, M.text, U.username ";
        sqlQuery += "FROM `Messages` AS `M`, `User` AS `U`, Chat AS C ";
        sqlQuery += "WHERE C.name LIKE '" + chatname + "'";
        sqlQuery += "AND C.id = M.chat ";
        sqlQuery += "AND U.id = M.user ";
        return sqlQuery;
    }
}
