package it.unibo.reactivechatservice.utils;

import java.io.IOException;
import java.net.DatagramSocket;
import java.net.ServerSocket;

public class CheckAvailablePorts {

    private static final int MIN_PORT_NUMBER = AppValues.CORE_PORT_NUMBER + 1;
    private static final int MAX_PORT_NUMBER = 65535;
    private final int supervisorNumber = ConfigReader.getSupervisorNumber();

    public boolean isPortAvailable(int port) {
        if (port < (MIN_PORT_NUMBER + supervisorNumber) || port > MAX_PORT_NUMBER) {
            throw new IllegalArgumentException("Invalid start port: " + port);
        }

        boolean available = false;
        ServerSocket ss = null;
        DatagramSocket ds = null;
        try {
            ss = new ServerSocket(port);
            ss.setReuseAddress(true);
            ds = new DatagramSocket(port);
            ds.setReuseAddress(true);
            available = true;
        } catch (IOException e) {
        } finally {
            if (ds != null) {
                ds.close();
            }
            if (ss != null) {
                try {
                    ss.close();
                } catch (IOException e) {
                    /* should not be thrown */
                }
            }
        }
        return available;
    }

    public int getFirstAvailablePort() {
        int port = MIN_PORT_NUMBER + supervisorNumber;
        while (!isPortAvailable(port) && port <= MAX_PORT_NUMBER) {
            port++;
        }
        if (port > MAX_PORT_NUMBER) {
            throw new IllegalStateException();
        }
        return port;
    }

}
