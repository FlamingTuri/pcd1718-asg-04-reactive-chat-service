package it.unibo.reactivechatservice.utils;

import io.vertx.core.AsyncResult;
import io.vertx.core.Handler;
import io.vertx.core.http.HttpServerResponse;
import io.vertx.ext.jdbc.JDBCClient;
import io.vertx.ext.sql.ResultSet;
import io.vertx.ext.sql.SQLConnection;

public class QueryUtils {

    public static void executeQuery(JDBCClient jdbcClient, String sqlQuery,
            Handler<AsyncResult<SQLConnection>> handler) {
        jdbcClient.getConnection(handler);
    }

    public static void executeQuery(JDBCClient jdbcClient, String sql, Handler<AsyncResult<ResultSet>> resultHandler,
            HttpServerResponse response) {
        jdbcClient.getConnection(res -> {
            if (res.succeeded()) {
                SQLConnection connection = res.result();
                connection.query(sql, resultHandler);
                connection.close();
            } else {
                ResponseHandler.dbConnectionFailMsg(response);
            }
        });
    }

    public static void executeQueryWithoutResponse(JDBCClient jdbcClient, String sql,
            Handler<AsyncResult<ResultSet>> resultHandler) {
        jdbcClient.getConnection(res -> {
            if (res.succeeded()) {
                SQLConnection connection = res.result();
                connection.query(sql, resultHandler);
                connection.close();
            } else {
                Logger.dbConnectionFailMsg();
            }
        });
    }

    public static void executeQueryWithoutResponse(SQLConnection connection, String sql) {
        connection.query(sql, queryResult -> {
            if (queryResult.succeeded()) {
                if (queryResult.result() != null) {
                    Logger.log(queryResult.result().toJson().encodePrettily());
                }
            } else {
                Logger.logQueryFailed(queryResult);
            }
        });
    }

}
