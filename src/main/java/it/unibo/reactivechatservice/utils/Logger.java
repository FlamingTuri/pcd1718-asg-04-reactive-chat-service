package it.unibo.reactivechatservice.utils;

import io.vertx.core.AsyncResult;
import io.vertx.ext.sql.ResultSet;

public class Logger {

    public static void log(String message) {
        System.out.println(message + "\n");
    }

    public static void dbConnectionFailMsg() {
        log(AppValues.DB_CONNECTION_ERROR_MSG);
    }
    
    public static void logQuerySuccedeed() {
        log("query succeeded");
    }

    public static void logQueryFailed(AsyncResult<ResultSet> queryResult) {
        log("query failed, cause " + queryResult.cause());
    }
}
