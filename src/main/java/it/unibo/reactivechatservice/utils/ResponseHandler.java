package it.unibo.reactivechatservice.utils;

import io.vertx.core.AsyncResult;
import io.vertx.core.http.HttpServerResponse;

public class ResponseHandler {

    public static void dbConnectionFailMsg(HttpServerResponse response) {
        Logger.dbConnectionFailMsg();
        sendError(400, response, AppValues.DB_CONNECTION_ERROR_MSG);
    }

    @SuppressWarnings({ "rawtypes", "unchecked" })
    public static void queryFailed(HttpServerResponse response, AsyncResult queryResult) {
        Logger.logQueryFailed(queryResult);
        sendError(400, response, queryResult.cause().getMessage());
    }

    public static void sendResponse(HttpServerResponse response, String responseText) {
        response.putHeader("Content-Type", "application/text").end(responseText);
    }

    public static void sendError(int statusCode, HttpServerResponse response) {
        response.setStatusCode(statusCode).end();
    }

    public static void sendError(int statusCode, HttpServerResponse response, String reason) {
        response.setStatusCode(statusCode);
        response.putHeader("Content-Type", "application/text");
        response.end(reason);
    }
}
