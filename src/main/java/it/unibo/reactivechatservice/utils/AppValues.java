package it.unibo.reactivechatservice.utils;

public class AppValues {

    public static final int CORE_PORT_NUMBER = 8080;
    public static final int RESPONSE_TIMEOUT = 5000;
    public static final int CHAT_LIVELINESS_MONITOR_INTERVAL = 10000;
    public static final int CRASH_MONITOR_INTERVAL = 20000;

    public static final String RESOURCE_PATH = "src/main/resources";
    public static final String CONFIG_PATH = RESOURCE_PATH + "/config";
    public static final String SCRIPT_PATH = RESOURCE_PATH + "/script";
    public static final String SUPERVISOR_NUMBER_FILE = "supervisorNumber.json";
    public static final String MYSQL_CONFIG_FILE = ".my.cnf";

    public static final String HOST = "localhost";
    public static final String IS_ALIVE_REST_API = "/isAlive";
    public static final String CHAT_READY_REST_API = "/chat/ready";
    public static final String DB_CONNECTION_ERROR_MSG = "Error: Failed getting connection with DB";

    public static final String RUN_CHAT_SERVICE = "./run-chat-service.sh";
    public static final String RUN_CORE_SERVICE = "./run-core-service.sh";
    public static final String RUN_CRASH_SUPERVISOR_SERVICE = "./run-crash-supervisor-service.sh";
    public static final String RUN_USER_CLIENT = "./run-user-client-from-ide.sh";

}
