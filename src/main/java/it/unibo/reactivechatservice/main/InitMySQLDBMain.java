package it.unibo.reactivechatservice.main;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.AsyncResult;
import io.vertx.core.Handler;
import io.vertx.core.Vertx;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.jdbc.JDBCClient;
import io.vertx.ext.sql.SQLConnection;
import it.unibo.reactivechatservice.utils.ConfigReader;
import it.unibo.reactivechatservice.utils.QueryFor;
import it.unibo.reactivechatservice.utils.AppValues;

public class InitMySQLDBMain extends AbstractVerticle {

    private final QueryFor queryFor = new QueryFor();
    private String sql;
    private JDBCClient jdbcClient;

    @Override
    public void start() throws Exception {
        JsonObject properties = ConfigReader.getJsonMysSqlConfig();
        jdbcClient = JDBCClient.createShared(vertx, properties);
        JDBCClient.createShared(vertx, properties);

        Handler<AsyncResult<SQLConnection>> handler = res -> {
            if (res.succeeded()) {

                SQLConnection connection = res.result();

                sql = queryFor.createDB();
                executeQueryWithoutResponse(connection, sql);

                sql = queryFor.createUserTable();
                executeQueryWithoutResponse(connection, sql);

                sql = queryFor.createChatTable();
                executeQueryWithoutResponse(connection, sql);

                connection.close();
            } else {
                dbConnectionFailMsg();
            }
        };
        jdbcClient.getConnection(handler);

        vertx.close();
    }

    private void executeQueryWithoutResponse(SQLConnection connection, String sqlQuery) {
        connection.query(sqlQuery, queryResult -> {
            if (queryResult.succeeded()) {
                if (queryResult.result() != null) {
                    log("" + queryResult.result().toJson());
                }
            } else {
                log("query failed");
                log("" + queryResult.cause());
            }
        });
    }

    private void dbConnectionFailMsg() {
        log(AppValues.DB_CONNECTION_ERROR_MSG);
    }

    private void log(String message) {
        System.out.println(message + "\n");
    }

    public static void main(String[] args) {
        InitMySQLDBMain verticle = new InitMySQLDBMain();
        Vertx vertx = Vertx.vertx();
        vertx.deployVerticle(verticle);
    }
}
