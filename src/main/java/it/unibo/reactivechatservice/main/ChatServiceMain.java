package it.unibo.reactivechatservice.main;

import io.vertx.core.Vertx;
import it.unibo.reactivechatservice.services.ChatService;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;

/**
 * Not working, java does not support socket.io
 */
public class ChatServiceMain extends Application {

    private Vertx vertx;

    @Override
    public void start(Stage stage) {
        VBox root = new VBox();
        Scene scene = new Scene(root, 500, 500);

        String chatname = System.getProperty("name");
        if (chatname == null) {
            System.exit(-1);
        }
        stage.setTitle("Chat " + chatname);
        stage.setScene(scene);

        stage.setOnCloseRequest((WindowEvent event) -> {
            vertx.close();
        });

        stage.widthProperty().addListener((obs, oldVal, newVal) -> {

        });

        stage.heightProperty().addListener((obs, oldVal, newVal) -> {

        });

        vertx = Vertx.vertx();
        ChatService service = new ChatService();
        vertx.deployVerticle(service);

        stage.show();
    }

    public static void main(String[] args) {
        launch(args);
    }
}
