package it.unibo.reactivechatservice.main;

import io.vertx.core.Vertx;
import it.unibo.reactivechatservice.services.CoreService;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;

public class CoreServiceMain extends Application {

    private Vertx vertx;

    @Override
    public void start(Stage stage) {
        VBox root = new VBox();
        Scene scene = new Scene(root, 500, 500);

        stage.setTitle("Register Service");
        stage.setScene(scene);

        stage.setOnCloseRequest((WindowEvent event) -> {
            if (notNull(vertx)) {
                vertx.close();
            }
        });

        stage.widthProperty().addListener((obs, oldVal, newVal) -> {

        });

        stage.heightProperty().addListener((obs, oldVal, newVal) -> {

        });

        vertx = Vertx.vertx();
        CoreService service = new CoreService();
        vertx.deployVerticle(service);

        stage.show();
    }

    private boolean notNull(Object o) {
        return o != null;
    }

    public static void main(String[] args) {
        // launch(args);
        try {
            Vertx vertx = Vertx.vertx();
            CoreService service = new CoreService();
            vertx.deployVerticle(service);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
