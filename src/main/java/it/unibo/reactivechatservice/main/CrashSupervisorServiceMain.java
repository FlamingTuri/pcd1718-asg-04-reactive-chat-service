package it.unibo.reactivechatservice.main;

import io.vertx.core.Vertx;
import it.unibo.reactivechatservice.services.CrashSupervisorService;

public class CrashSupervisorServiceMain {

    public static void main(String[] args) {
        try {
            int port = Integer.parseInt(args[0]);
            CrashSupervisorService crashSupervisor = new CrashSupervisorService(port);
            Vertx vertx = Vertx.vertx();
            vertx.deployVerticle(crashSupervisor);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
