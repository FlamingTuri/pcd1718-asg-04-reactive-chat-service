package it.unibo.reactivechatservice.main;

import java.io.File;
import java.io.IOException;

import it.unibo.reactivechatservice.utils.CheckAvailablePorts;
import it.unibo.reactivechatservice.utils.AppValues;

public class UserClientMain {

    public static void main(String[] args) {
        CheckAvailablePorts checkAvailablePort = new CheckAvailablePorts();
        String port = "" + checkAvailablePort.getFirstAvailablePort();
        try {
            Runtime runTime = Runtime.getRuntime();
            String[] cmdarray = { AppValues.RUN_USER_CLIENT, port };
            runTime.exec(cmdarray, null, new File(AppValues.SCRIPT_PATH));
            System.out.println("started new user client");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
