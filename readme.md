# Project done during A.A. 2017/2018 for Programmazione Concorrente e Distribuita course


Before running the project remember to:

+ run `npm install` on both chat-service and user-service at src/main/resources/webroot/
+ import the db at src/main/resources/config/
+ you need port 8080 free to run the COreService
  

It is also suggested running `gradle build` to avoid building java based services during the app execution

## Gradle commands

+ run `./gradle runCrashSupervisorService` to start the CrashSupervisorService
+ run `./gradle runCoreService` to start the CoreService
+ run `.gradle runUserClient` to start the Angular 2 app

